/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.componentssample.model;

/**
 *
 * @author student
 */
public class MovieDetail {
    
    private Integer id;
    private String name;
    private String screenNo;

    public MovieDetail(Integer id, String name, String screenNo) {
        this.id = id;
        this.name = name;
        this.screenNo = screenNo;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenNo() {
        return screenNo;
    }

    public void setScreenNo(String screenNo) {
        this.screenNo = screenNo;
    }

    @Override
    public String toString() {
        return name+"-" + screenNo;
    }
    
    
}
